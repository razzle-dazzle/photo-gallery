const express = require('express');
const {
    uploadPhoto,
    createGallery,
    getGalleries
} = require('../controllers/galleries');

const { protect, authorize } = require('../middleware/auth')

const router = express.Router();


router.route('/')
    .post(protect, authorize('user','admin') ,createGallery)
    .get(getGalleries);

router.put('/:id/photo',protect, authorize('user','admin') , uploadPhoto);

module.exports = router;