const express = require('express');
const {
    getUser,
    createUser,
    deleteUser
} = require('../controllers/users');

const User = require('../models/User');

const { protect, authorize } = require('../middleware/auth');

const router = express.Router();

router.route('/')
    .post(createUser);
router.route('/:id')
    .get(protect, authorize('user'),getUser)
    .delete(protect, authorize('admin'),deleteUser)

module.exports = router;