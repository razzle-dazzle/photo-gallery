
(async () => {
// NPM
const path = require('path');
const express = require('express');
const morgan = require('morgan');
const colors= require('colors');
const dotenv= require('dotenv');
const fileupload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const xss = require('xss-clean');
const mongoSanitize = require('express-mongo-sanitize');
const hpp = require('hpp');
const cors = require('cors');
const expressRateLimit = require('express-rate-limit');

//Middleware
const errorHandler = require('./middleware/error');


dotenv.config({ path: './config/.env' });

const connectDB= require('./config/db');
connectDB();

const app = express();

// @TODO
// Import Routes
const users = require("./routes/users");
const auth = require("./routes/auth");
const galleries = require("./routes/galleries");

// Dev logging middleware
if(process.env.NODE_ENV === 'development'){
    app.use(morgan('dev'));
}

// Body parser
app.use(express.json());

// Cookie parser
app.use(cookieParser());

// Sanitize data
app.use(mongoSanitize());

// Set security headers
app.use(helmet());

// Prevent XSS
app.use(xss());

// @TODO
// Move to Nginx
// Rate limiting
const limiter = expressRateLimit({
  max: 100,
  windowMs: 2 * 60 * 1000 // 2 mins
});

app.use(limiter);

// Prevent http param polution
app.use(hpp());

// Move to Nginx
// Enable cors
app.use(cors());



// File uploading
app.use(fileupload());
// Static folder for photos
// @TODO
// Handler for storage location
app.use(express.static(path.join(__dirname,'public')));

const port = process.env.port || 3000

// @TODO
// Add routes to express
app.use('/api/v1/users', users);
app.use('/api/v1/auth', auth);
app.use('/api/v1/galleries', galleries);

// Handle centrally caught errors.
app.use(errorHandler);


const server = app.listen(port, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${port}`.rainbow.bold))

process.on('uncaughtException', (err, promise) => {
    console.log(`Error: ${err.message}`.red);
    //Close server & exit process
    server.close(()=> process.exit(1))
});

})();