const mongoose = require('mongoose');
const PhotoSchema = require('./Photo');

const GallerySchema = new mongoose.Schema({
    name : {
        type: String,
        required: [true, 'Please add a name'],
        trim: true,
        maxlength: [50, 'Name cannot be more than 50 characters']
    },
    photos: {
        type: [PhotoSchema]
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = mongoose.model('Gallery', GallerySchema);