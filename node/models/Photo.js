
const mongoose = require('mongoose');
// Photo structure
const PhotoSchema = new mongoose.Schema(
    {   thumbnail: { 
            type :String,
            default: "no-thumb.jpg"
        },
        photo: { 
            type: String,
            required: true
        },
        createdAt: {
            type : Date,
            default: Date.now
        }
    },
    { _id: false } // Stop creating indexes
    );

module.exports = PhotoSchema;
