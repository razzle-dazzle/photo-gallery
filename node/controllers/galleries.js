const path = require('path');
const fs = require('fs');
const imageThumbnail= require('image-thumbnail');
const ErrorResponse = require('../utils/errorResponse');
const Gallery = require('../models/Gallery');
const asyncHandler = require('../middleware/async');
const cryptoRandomString = require('crypto-random-string')

// @desc        Get galleries
// @route       GET /api/v1/galleries
// @access      Public

exports.getGalleries = asyncHandler(async (req, res , next ) => {
    // @TODO
    // remove photos from galleries
    const galleries = await Gallery.find({});

    res
        .status(200)
        .json({ success: true, data: galleries})

});

// @desc        Create new gallery
// @route       POST api/v1/galleries
// @access      Private

exports.createGallery = asyncHandler(async (req, res , next ) => {
    // Add user to req.body
    req.body.user = req.user.id;
    req.body.photos = undefined;

    const gallery = await Gallery.create(req.body);

    if(!gallery)
        return next(new ErrorResponse(`Gallery not found with id of ${req.params.id}`, 404));

    res
        .status(201)
        .json({ 
            success: true, 
            msg: `Create new gallery`,
            data: gallery
        })
    
});

// @desc        Update photo in gallery
// @route       POST /api/v1/galleries/:id/photo
// @access      Private

exports.uploadPhoto = asyncHandler(async (req, res , next ) => {
    
    const gallery = await Gallery.findById(req.params.id);

    if(!gallery)
        return next(new ErrorResponse(`Gallery not found with id of ${req.params.id}`, 404));
    
    // Make sure user is gallery owner
    if(gallery.user.toString() != req.user.id){
        return next(new ErrorResponse(`User ${req.user.id} is not authorized to update this gallery`, 401));
    }

    if(!req.files){
        return next(new ErrorResponse(`Please upload a file`, 400));
    }

    const file = req.files.file;
    
    // Make sure image is a photo.
    if(!file.mimetype.startsWith('image')){
        return next(new ErrorResponse(`Please upload an image file`, 400));
    }

    if(file.size > process.env.MAX_FILE_UPLOAD){
        return next(new ErrorResponse(`Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`, 400));
    }

    // Create custom file names
    // Eg photo_randomString.jpg
    const randomString = cryptoRandomString(10);
    
    file.name = `photo_${randomString}${path.parse(file.name).ext}`;
    let fileLocation =`${process.env.FILE_UPLOAD_PATH}/${file.name}`;
    // Eg photo_id-thumb.jpg
    let thumbName = `thumb_${randomString}${path.parse(file.name).ext}`;
    let thumbLocation = `${process.env.FILE_UPLOAD_PATH}/${thumbName}`

    // @TODO
    // Add handlers for different storage locations (HOST or S3)
    // Send file to docker image
    file.mv(fileLocation, async err => {
        if(err){
            console.log(err);
            return next(new ErrorResponse(`Problem with file upload`, 500));
        }

        let photoObj = { photo: file.name };
        let options = { width: 50, height: 50, responseType: 'buffer' };
        
        // Make thumbnail from FS
        const thumbnail = await imageThumbnail(`${process.env.FILE_UPLOAD_PATH}/${file.name}`,options);

        // Save thumbnail to FS

        fs.writeFileSync(thumbLocation, thumbnail);
        photoObj.thumbnail = thumbName;
        // Add to gallery
        await Gallery.findByIdAndUpdate(req.params.id,{
                $push: { photos: photoObj } 
        });
    })

    res
        .status(200)
        .json({ success: true, data: file.name });

});