const ErrorResponse = require('../utils/errorResponse');
const User = require('../models/User');
const asyncHandler = require('../middleware/async');

// @desc        Create user
// @route       POST api/v1/users
// @access      Public

exports.createUser = asyncHandler ( async(req, res, next) => {

    // Stop users from becoming admins.
    req.body.role = undefined;
    const user = await User.create(req.body)
    // Returns token
    res
        .status(201)
        .json({
            success:true,
            data: user
    });
 
});

// @desc        Get single user
// @route       GET api/v1/users/:id
// @access      User

exports.getUser = asyncHandler ( async(req, res, next) => {

    const user = await User.findById(req.params.id)
    res
        .status(200)
        .json({
            success:true,
            data: user
    });
 
});

// @desc        Delete user
// @route       DELETE api/v1/users/:id
// @access      Private/Admin

exports.deleteUser = asyncHandler ( async(req, res, next) => {

    await User.findByIdAndDelete(req.params.id);

    res
        .status(201)
        .json({
            success:true,
            data: {}
    });
 
});