class ErrorResponse extends Error{
    constructor(message, statusCode, corellationId) {
        super(message);
        this.statusCode = statusCode;
        this.corellationId= corellationId;
    }
}

module.exports = ErrorResponse;